#!/bin/bash

DIR="/home/leland/rankings/"

Rscript -e "rmarkdown::render('$DIR/nba_rankings.Rmd')"
/home/leland/anaconda2/bin/python $DIR/send_rankings.py
