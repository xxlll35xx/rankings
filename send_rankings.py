#!/usr/bin/env python

import json
import subprocess
from datetime import datetime
from exceptions import ValueError
import re
import os
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from twilio.rest import TwilioRestClient as Client

"""
Send email code from:
http://naelshiab.com/tutorial-send-email-python/
"""
DIR = '/home/leland/rankings/'

def send_email(html_file):
	fromaddr = "leland.lockhart@utexas.edu"
	toaddr = "leland.lockhart@utexas.edu"
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "NBA Rankings for %s" % datetime.strftime(datetime.now(), "%Y-%m-%d")


	with open(html_file,'rb') as f:
		html_to_send = MIMEText(f.read())
	html_to_send.add_header("Content-Disposition","attachment",filename=html_file)
	msg.attach(html_to_send)

	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, "clyqjydngddqtdvv")
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()

def send_sms(message, prefix = '', to_list = []):
	account_sid = "AC27d311bf005095ce1665a3eba5e9aeb5"
	auth_token  = "b0dba060c6799f7610c4142e5f30a0de"

	client = Client(account_sid, auth_token)

	for phone in to_list:
		to_send = client.messages.create(
			to=phone,
			from_="+18303550350",
			body=prefix + '\n\n' + message)
	
		print "%s-%s" % ("SMS Sent",to_send.sid)

if __name__ == '__main__':
	send_email(DIR + 'nba_rankings.html')





