library(lme4)

lockhart <- function(df) {
  
  model <- glmer(y ~ 0 + (1 | team) + (1 | opponent),
                 data = df, family = binomial(link = 'logit'), 
                 nAGQ=1)
  
  thetas <- ranef(model, condVar = T)$team
  thetas$team <- row.names(thetas)
  row.names(thetas) <- NULL
  
  rankings <- cbind(1:nrow(thetas), thetas[order(thetas[,1], decreasing = T),])
  names(rankings) <- c('rank_lockhart','theta','team')
 
  return(rankings)
}