R implementations of various methods for rating and ranking.
Also contains scripts to download current sports data on which to apply ranking methods.
Still a work in progess.

Descriptions of each method can be found in:
Who's #1?: The Science of Rating and Ranking
by Amy N. Langville, Carl D. Meyer