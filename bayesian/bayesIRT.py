# References:
# http://nbviewer.ipython.org/github/CamDavidsonPilon/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/blob/master/Chapter6_Priorities/Chapter6.ipynb
# http://twiecki.github.io/blog/2014/03/17/bayesian-glms-3/

import matplotlib.pyplot as plt
import numpy as np
import pymc3 as pm 
import pandas as pd
from pymc3.stats import hpd

# read data
SEASON = 2016
data = pd.read_csv('CFB' + str(SEASON) + '_regular_season.psv', delimiter = "|", index_col = None)

# list of unique team names
team_names = set(data.team1.unique().tolist() + data.team2.unique().tolist())

n_team1 = len(data.team1.unique())
n_team2 = len(data.team2.unique())

y = data.y

# code team names
team1_idx = pd.Categorical(data['team1']).codes
team2_idx = pd.Categorical(data['team2']).codes

# create team-idx lookup
team_dict = {}
for i,team in enumerate(data.team1):
	if team_dict.has_key(team):
		continue
	team_dict[team] = team1_idx[i]

with pm.Model() as hierarchical_model:
	# Hyperpriors for group nodes
	mu_theta = pm.Normal('mu_theta', mu=0., sd=1)
	tau_theta = pm.Gamma('tau_theta', alpha=2, beta=0.5)
	mu_beta = pm.Normal('mu_beta', mu=0., sd=1)
	tau_beta = pm.Gamma('tau_beta', alpha=2, beta=0.5)
	# theta and difficulty parameters
	theta = pm.Normal('theta', mu=mu_theta, tau=tau_theta, shape=n_team1)
	b = pm.Normal('beta', mu=mu_beta, tau=tau_beta, shape=n_team2)

	sigma_theta = tau_theta**-2
	sigma_beta = tau_beta**-2

	logit_y = theta[team1_idx] - b[team2_idx]
	p = pm.exp(logit_y)/(1 + pm.exp(logit_y))
	
	observed = pm.Bernoulli('observed', p, observed = y)


# run MCMC
with hierarchical_model:
	start = pm.find_MAP()
	step = pm.NUTS(scaling = start)
	trace = pm.sample(2000, step, start = start, progressbar = True, njobs = -1)

# plots
#fig = pm.plots.traceplot(trace);
#plt.show(fig)

# mean ability calcs
abilities = {}
for t in team_dict.keys():
	idx = team_dict[t]
	#bounds = hpd(trace['theta'][1000:,idx])[0]
	current_ability = np.mean(trace['theta'][1000:,idx])
	abilities[t] = current_ability

# print ranks to screen
ranks = {}
from operator import itemgetter
for i, r in enumerate(sorted(abilities.items(), key=itemgetter(1), reverse = True)):
		#print i+1, "\t", r
		#ranks[r[0]] = {'rank':i+1, 'theta':r[1]}
		print "|".join([str(i+1),r[0],str(r[1])])

##########################################
##########################################
###### comparison utility funcitons #################

class Comparison(object):
	def __init__(self, trace, team_dict):
		self.trace = trace
		self.team_dict = team_dict

	def get_team_index(self, team):
		return self.team_dict[team]

	def get_team_trace(self, team):
		idx = self.get_team_index(team)
		return self.trace['theta'][1000:,idx]

	def comparison(self, team1, team2):
		theta1 = self.get_team_trace(team1)
		theta2 = self.get_team_trace(team2)
		pct = np.mean(theta1 > theta2)
		return pct

comp = Comparison(trace, team_dict)
comp.comparison('Alabama','Clemson')

big12 = ['Oklahoma','Texas Tech','Texas Christian','West Virginia','Oklahoma State','Baylor','Iowa State', 'Kansas','Kansas State']

for opp in big12:
	comp.comparison('Texas',opp)

##########################################
##########################################
########### apply to bowl games ##########

bowls = pd.read_csv('CFB2016_bowls.psv', delimiter = "|", index_col = None)
for idx, row in bowls.iterrows():
	pct = comp.comparison(row['team1'], row['team2'])
	bowls.loc[idx, 'pct'] = pct
	thresh = 0.75
	if pct >= thresh:
		bowls.loc[idx, 'pred'] = row['team1']
	elif pct < (1 - thresh):
		bowls.loc[idx, 'pred'] = row['team2']
	else:
		bowls.loc[idx, 'pred'] = "No Prediction"


subset = bowls[bowls.pred != 'No Prediction']
np.mean(subset.team1 == subset.pred)

