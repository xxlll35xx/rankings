# Markov/PageRank rankings

create_markov_matrix <- function(df, team1, team2, stat, pagerank = F) {
  # set pagerank to T if using wins and losses
  
  teams <- unique(c(as.character(df[[team1]]), as.character(df[[team2]])))
  teams <- teams[order(teams)]
  
  markov_matrix <- matrix(0, nrow = length(teams), ncol = length(teams))
  rownames(markov_matrix) <- colnames(markov_matrix) <- teams
  
  ngames <- rep(0, length(teams))
  names(ngames) <- teams
  
  for(i in 1:nrow(df)){
    tmp <- df[i,]
    markov_matrix[tmp[[team1]], tmp[[team2]]] <- markov_matrix[tmp[[team1]], tmp[[team2]]] + tmp[[stat]]
    ngames[tmp[[team1]]] <- ngames[tmp[[team1]]] + 1
  }
  
  markov_matrix <- markov_matrix/rowSums(markov_matrix)
  
  for(i in 1:length(teams)){
    if(is.nan(sum(markov_matrix[teams[i], ]))){
      if(pagerank) {
        markov_matrix[teams[i], ] <- rep(0, ncol(markov_matrix))
      } else {
        markov_matrix[teams[i], ] <- rep(1/ncol(markov_matrix), ncol(markov_matrix))
      }
    }
  }
    
  return(markov_matrix)
  
}

create_pagerank_matrix <- function(mat, beta = 0.85) {
  M <- (beta * mat)
  U <- ((1 - beta) * matrix(1/nrow(mat), nrow(mat), ncol(mat)))
  S <-  M + U
  return(S)
}

markov <- function(df, team1, team2, statistic, ...) {
  require(DTMCPack)
  teams <- unique(c(df[[team1]], df[[team2]]))
  markov_matrix <- create_markov_matrix(df, team1, team2, statistic, pagerank = T)
  pagerank_matrix <- create_pagerank_matrix(markov_matrix, ...)
  static_distr <- statdistr(pagerank_matrix)
  static_distr2 <- as.vector(static_distr)
  names(static_distr2) <- colnames(static_distr)
  markov_rankings <- data.frame(team = names(static_distr2), 
                                markov = static_distr2,
                                row.names = NULL)
  markov_rankings <- markov_rankings[order(markov_rankings$markov, decreasing = F),]
  markov_rankings$rank_markov <- 1:length(teams)
  return(markov_rankings)
}
